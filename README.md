# vlc-all2mp3
A small script convert any video or audio file to MP3 using VLC and Lame

**Required softwares:**
- [VLC](https://www.videolan.org/vlc/)
- [LAME](http://lame.sourceforge.net/index.php)
